package org.openiot.scheduler.core.gadgeon;




import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import org.openiot.commons.util.PropertyManagement;
import org.openiot.lsm.server.LSMTripleStore;
import org.openiot.scheduler.core.utils.sparql.SesameSPARQLClient;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryException;



public class SensorDelete {

	
	public static LSMTripleStore lsmStore = new LSMTripleStore("http://lsm.deri.ie/lsm-light.server/");
    private String graphurl;
	private String lsmMetaGraph = "";
	private String sensorurl;
	private String replyMessage= "";
		public  SensorDelete(String graphurl,String sensorurl) throws QueryEvaluationException
	     {
	    	 PropertyManagement propertyManagement = new PropertyManagement();		
	    		lsmMetaGraph = propertyManagement.getSchedulerLsmMetaGraph();
	    	 this.graphurl= graphurl;
	    	 this.sensorurl= sensorurl;
 
		     //Object list= lsmStore.geonhistorydatademo(sensortype);
	    	 gadhistory();
	     }
		
		public String getReplyMessage()
		{	
			return replyMessage;
		}
		
	     public void gadhistory() throws QueryEvaluationException
	     {
	    		SesameSPARQLClient sparqlCl = null;
	    		try {
	    			sparqlCl = new SesameSPARQLClient();
	    	
	            ArrayList<Object> arr = new ArrayList<>();

	    		TupleQueryResult qres = sparqlCl.sparqlToQResult(deleteSensor(graphurl,sensorurl));
	    		replyMessage= "Sensor Deleted Successfully";
	    		} catch (RepositoryException e) {
		    		//	logger.error("Init sparql repository error. Returning an empty SensorTypes object. ", e);
		    		replyMessage= "Sensor Deleted UnSuccessfull";
			
	    			return;
		    			
		    		}
	    	 
	       
	      
	     }
	    
	     public String deleteSensor(String graphURL,String sensorURL) {
	         // TODO Auto-generated method stub
	         //System.out.println("##########"+sensorURL +fromTime);
				StringBuilder update = new StringBuilder();

				String str = "DELETE from <"+ graphURL +">{" +
						"<"+sensorURL + "> ?p ?o.}" + 			
						"where{<"+sensorURL + "> ?p ?o." +							     	
						"}";				  
	        
	         	update.append(str);
				return update.toString();
	     }
		
}
