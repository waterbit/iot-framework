package org.openiot.scheduler.core.gadgeon;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Set;

import org.openiot.commons.util.PropertyManagement;
import org.openiot.lsm.server.LSMTripleStore;
import org.openiot.scheduler.core.utils.sparql.SesameSPARQLClient;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryException;

import arq.update;



public class OffsetHistory {

	
	public static LSMTripleStore lsmStore = new LSMTripleStore("http://lsm.deri.ie/lsm-light.server/");
    private String sensortype;
    private String fromdate;
    private String todate;
    private String offset;
	private String lsmMetaGraph = "";
	private Object history;
	     public Object getHistorydate() {
		return history;
	}
	public void setHistorydate(Object history) {
		this.history = history;
	}
		public  OffsetHistory(String sensortype1,String fromdate ,String todate,String offset) throws Exception
	     {
	    	 PropertyManagement propertyManagement = new PropertyManagement();		
	    		lsmMetaGraph = propertyManagement.getSchedulerLsmMetaGraph();
	    	 this.sensortype= sensortype1;
	    	 this.fromdate=fromdate;
	    	 this.todate=todate;
	    	 this.offset=offset;
		     //Object list= lsmStore.geonhistorydatademo(sensortype);
	    	 gadhistorydate();
	     }
	     public void gadhistorydate() throws QueryEvaluationException, Exception
	     {
	    		SesameSPARQLClient sparqlCl = null;int cnt_i=0;
	    		try {
	    			sparqlCl = new SesameSPARQLClient();
	    		} catch (RepositoryException e) {
	    		//	logger.error("Init sparql repository error. Returning an empty SensorTypes object. ", e);
	    			return;
	    		}
	            ArrayList<Object> arr = new ArrayList<>();

	    		TupleQueryResult qres = sparqlCl.sparqlToQResult(getSensorHistoricalDatabetweendate(sensortype,fromdate,todate,offset));
	    		String history =null;
	    	      //	ArrayList<String>list = new ArrayList<>();
	    	     LinkedHashMap<String, String>   list =null; 
	                  while (qres.hasNext()) {  // iterate over the result
	                	  list = new LinkedHashMap<>();
	    			BindingSet bindingSet = qres.next();
	    			Value valueOfX = bindingSet.getValue("x1");
	    			Value valueOfY = bindingSet.getValue("y1");
	    			list.put("x1",valueOfX.stringValue());
	    			list.put("y1", valueOfY.stringValue());
	    			//System.out.println("**********"+list);
				cnt_i++;
	    			 arr.add(list);
	                  }

				System.out.println("**********"+cnt_i);
	                  setHistorydate(arr);
	
	    	 
	       
	      
	     }
	    
	     public String getSensorHistoricalDatabetweendate(String sensorURL, String fromdate ,String todate,String offset) throws ParseException {

	    	    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

				StringBuilder update = new StringBuilder();

	    	        Date datefrom = formatter.parse(fromdate);
	    	        Date dateto = formatter.parse(todate);

	    	    Connection conn = null;

	    	    LinkedHashMap<String, String> reading = new LinkedHashMap<>();
	    	    ArrayList<Object> arr = new ArrayList<>();
	    	/*    String query = "sparql select ?s ?type ?name ?value ?time "+
	    	            " from <"+ "http://gadgeon.com/OpenIoT/sensordata#" +"> " +
	    	                "where{ "+
	    	                  "{ "+
	    	                   "select ?observation where "+
	    	                     "{ "+
	    	                       "?observation <http://purl.oclc.org/NET/ssnx/ssn#observedBy> <"+sensorURL+">. "+
	    	                       "?observation <http://purl.oclc.org/NET/ssnx/ssn#observationResultTime> ?time. "+
	    	                    "filter( ?time >\""+DateUtil.date2StandardString(fromTime)+"\"^^xsd:dateTime).} "+
	    	                  "} "+
	    	                  "?s <http://lsm.deri.ie/ont/lsm.owl#isObservedPropertyOf> ?observation. "+
	    	                  "?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?type. "+
	    	                  "?s <http://lsm.deri.ie/ont/lsm.owl#value> ?value."+
	    	                  "?s <http://purl.oclc.org/NET/ssnx/ssn#observationResultTime> ?time. "+
	    	                  "OPTIONAL{?s <http://www.w3.org/2000/01/rdf-schema#label> ?name.}"+
	    	                "}";        */
	    	    String UIDn = "graphNodeEndpoint_" + System.nanoTime();
	    	    String UIDnde = "graphNode_" + System.nanoTime();
	    	    String str = new StringBuilder()
	    	         .append("SELECT DISTINCT  ?x1 ?y1\n")
	    	         .append("FROM <http://gadgeon.com/OpenIoT/sensordata#>\n")
	    	         .append("WHERE\n")
	    	         .append("{\n")
	    	         .append("{\n")
	    	         .append("SELECT ?"+UIDnde+"_recordTime AS ?x1 ?"+UIDn+" AS ?y1\n")
	    	         .append("WHERE\n")
	    	         .append("{\n")
	    	         .append("?"+UIDnde+"_record <http://lsm.deri.ie/ont/lsm.owl#value> ?"+UIDn+" .\n")
	    	        // .append("?"+UIDnde+"_record <http://www.w3.org/2000/01/rdf-schema#label> 'Temperature' .\n")
	    	         .append("?"+UIDnde+"_record <http://purl.oclc.org/NET/ssnx/ssn#observationResultTime> ?"+UIDnde+"_recordTime .\n")
	    	         .append("?"+UIDnde+"_record <http://lsm.deri.ie/ont/lsm.owl#isObservedPropertyOf> ?"+UIDnde+"_sensor .\n")
	    	         .append("?"+UIDnde+"_sensor <http://purl.oclc.org/NET/ssnx/ssn#observedBy> ?"+UIDnde+"_sensorId .\n")
	    	         .append("{\n")
	    	         .append("SELECT ?"+UIDnde+"_sensorId\n")
	    	         .append("FROM <http://gadgeon.com/OpenIoT/sensormeta#>\n")
	    	         .append("WHERE\n")
	    	         .append("{\n")
	    	         .append("?"+UIDnde+"_sensorId <http://lsm.deri.ie/ont/lsm.owl#hasSensorType> ?"+UIDnde+"_sensorType .\n")
	    	         .append("?"+UIDnde+"_sensorType <http://www.w3.org/2000/01/rdf-schema#label> '"+sensorURL+"' .\n")
	    	         .append("?"+UIDnde+"_sensorId <http://www.loa-cnr.it/ontologies/DUL.owl#hasLocation> ?"+UIDnde+"_loc .\n")
	    	         .append("?"+UIDnde+"_loc geo:geometry ?"+UIDnde+"_geo .\n")
	    	         .append("?"+UIDnde+"_loc geo:lat ?"+UIDnde+"_lat .\n")
	    	         .append("?"+UIDnde+"_loc geo:long ?"+UIDnde+"_lon .\n")
	    	         //.append("FILTER (<bif:st_intersects>(?"+UIDnde+"_geo, <bif:st_point>( 76.33257865905762, 10.004843543350164), 15.0)) .\n")
	    	         .append("}\n")
	    	         .append("}\n")
	    	         .append("FILTER (?"+UIDnde+"_recordTime >=\""+DateUtil.date2StandardString(datefrom)+"\"^^xsd:date AND ?"+UIDnde+"_recordTime <=\""+DateUtil.date2StandardString(dateto)+"\"^^xsd:date ) .\n")
	    	        .append("}\n")
	    	         .append("ORDER BY ?"+UIDnde+"_recordTime\n")
	    	         .append("}\n")
	    	          .append("}\n")
	    	          .append("LIMIT 1000\n")
	    	          .append("OFFSET "+offset+"\n")
	    	         .toString();
	    	          update.append(str);
                         System.out.println("shamily===>>"+str);

				    return update.toString();
	     }
}
