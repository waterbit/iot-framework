

package org.openiot.scheduler.core.gadgeon;




import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Set;

import org.openiot.commons.util.PropertyManagement;
import org.openiot.lsm.server.LSMTripleStore;
import org.openiot.scheduler.core.utils.sparql.SesameSPARQLClient;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryException;



public class DeleteByDate {

	
	public static LSMTripleStore lsmStore = new LSMTripleStore("http://lsm.deri.ie/lsm-light.server/");
    private String graphurl;
	private String lsmMetaGraph = "";
	private String sensorurl;
	private String replyMessage= "";
	private String fromdate;
	private String todate;
		public  DeleteByDate(String graphurl,String sensorurl,String fromdate,String todate) throws QueryEvaluationException
	     {
	    	 PropertyManagement propertyManagement = new PropertyManagement();		
	    		lsmMetaGraph = propertyManagement.getSchedulerLsmMetaGraph();
	    	 this.graphurl= graphurl;
	    	 this.sensorurl= sensorurl;
	    	 this.fromdate=fromdate;
	    	 this.todate=todate;
	    	 
 
		     //Object list= lsmStore.geonhistorydatademo(sensortype);
	    	 gadhistory();
	     }
		public String getReplyMessage()
		{	
			return replyMessage;
		}
	     public void gadhistory() throws QueryEvaluationException
	     {
	    		SesameSPARQLClient sparqlCl = null;
	    		try {
	    			sparqlCl = new SesameSPARQLClient();
		    		TupleQueryResult qres = sparqlCl.sparqlToQResult(deleteSensorbydate(graphurl,sensorurl,fromdate,todate));
		    		replyMessage="All triples were deleted";
	    		} catch (Exception e) {
	    		//	logger.error("Init sparql repository error. Returning an empty SensorTypes object. ", e);
	    			replyMessage="	fail to execute query ";
	    			return;
	    			
	    		}

	    	
	    	 
	       
	      
	     }
	    
	     public String deleteSensorbydate(String graphURL,String sensorURL,String fromdate ,String todate) throws ParseException {
	         // TODO Auto-generated method stub
	         //System.out.println("##########"+sensorURL +fromTime);
	    	   SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

				StringBuilder update = new StringBuilder();
	        Date datefrom = formatter.parse(fromdate);
	    	        Date dateto = formatter.parse(todate);

				String	str="";
				if(todate!=null){
				str = "sparql delete from <"+ sensorURL+"> {?s ?p ?o} "+
						"where{ "+
							"{ {"+
									"?observation <http://purl.oclc.org/NET/ssnx/ssn#observedBy>  <"+sensorURL+">."+  						
									"?observation <http://purl.oclc.org/NET/ssnx/ssn#observationResultTime> ?time."+
								    "filter( ?time "+">="+" \""+DateUtil.date2StandardString(datefrom)+"\"^^xsd:dateTime && "+
								    "?time <= \""+DateUtil.date2StandardString(dateto)+"\"^^xsd:dateTime)."+
								 "}"+
								"?s <http://lsm.deri.ie/ont/lsm.owl#isObservedPropertyOf> ?observation."+
								"?s ?p ?o."+
							"}"+
						"union{ "+
							"?s <http://purl.oclc.org/NET/ssnx/ssn#observedBy>  <"+sensorURL+">."+
							"?s ?p  ?o."+
						"}"+
					"}";
				}else{
					str = "sparql delete from <"+ sensorURL+"> {?s ?p ?o} "+
							"where{ "+
								"{ {"+
										"?observation <http://purl.oclc.org/NET/ssnx/ssn#observedBy>  <"+sensorURL+">."+  						
										"?observation <http://purl.oclc.org/NET/ssnx/ssn#observationResultTime> ?time."+
									    "filter( ?time "+"<="+" \""+DateUtil.date2StandardString(datefrom)+"\"^^xsd:dateTime)."+
									 "}"+
									"?s <http://lsm.deri.ie/ont/lsm.owl#isObservedPropertyOf> ?observation."+
									"?s ?p ?o."+
								"}"+
							"union{ "+
								"?s <http://purl.oclc.org/NET/ssnx/ssn#observedBy>  <"+sensorURL+">."+
								"?s ?p  ?o."+
							"}"+
						"}";					
				}
	         	update.append(str);
				return update.toString();
	     }
		
}
