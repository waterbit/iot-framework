package org.openiot.gsn.wrappers.gadgeon;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.openiot.gsn.beans.AddressBean;
import org.openiot.gsn.beans.DataField;
import org.openiot.gsn.beans.StreamElement;
import org.openiot.gsn.metadata.LSM.LSMSensorMetaData;
import org.openiot.gsn.utils.CaseInsensitiveComparator;
import org.openiot.gsn.wrappers.AbstractWrapper;
import org.openiot.gsn.wrappers.gadgeon.DataListener;
import org.openiot.gsn.wrappers.general.CSVHandler;

import au.com.bytecode.opencsv.CSVReader;

public class GadgeonRestWrapper extends AbstractWrapper implements DataListener {
	private static Logger logger = Logger.getLogger(CSVHandler.class);
	private String[] fields, formats;
	private String sensornames;
	private DataField[] dataFields;
	private ArrayList<JSONObject> dataBuffer = new ArrayList<JSONObject>();
	LSMSensorMetaData lsmd = new LSMSensorMetaData();
	String tempValue="";

	public long manaf=-1;
	
	@Override
	public boolean initialize() {
		// TODO Auto-generated method stub
		AddressBean addressBean = getActiveAddressBean();

		try {
			this.fields = generateFieldIdx(
					addressBean.getPredicateValueWithException("fields"), true);
			this.formats = generateFieldIdx(
					addressBean.getPredicateValueWithException("formats"),
					false);
			this.sensornames =	addressBean.getVirtualSensorName();
			
		} catch (IOException e) {
			// TDO Auto-generated catch block
			logger.error(e.getMessage(), e);
			return false;
		}

		if (!validateFormats(this.formats)) {
			return false;
		}
		if (fields.length != formats.length) {
			logger.error("loading the Gadgeon REST wrapper failed as the "
					+ "length of fields(" + fields.length
					+ ") doesn't match the length of formats(" + formats.length
					+ ")");
			return false;
		}
		dataFields = getDataFields();

		// Initialize Event sources
		// eventSources.add(new GadgeonRestServer());

		return true;
	}

	private static String[] generateFieldIdx(String rawFields,
			boolean toLowerCase) throws IOException {
		String[] toReturn = new CSVReader(new StringReader(rawFields))
				.readNext();
		if (toReturn == null) {
			return new String[0];
		}
		for (int i = 0; i < toReturn.length; i++) {
			toReturn[i] = toReturn[i].trim();
			if (toLowerCase) {
				toReturn[i] = toReturn[i].toLowerCase();
			}
		}
		return toReturn;
	}

	public static boolean validateFormats(String[] formats) {
		for (String format : formats) {
			if (format.equalsIgnoreCase("numeric")
					|| format.equalsIgnoreCase("string")
					|| format.equalsIgnoreCase("timestamp")) {
				continue;
			} else {
				logger.error("The format (" + format
						+ ") used by the CSV-Wrapper doesn't exist.");
				return false;
			}
		}
		return true;

	}

	public String[] getFields() {
		return fields;
	}

	public String[] getFormats() {
		return formats;
	}

	public DataField[] getDataFields() {
		HashMap<String, String> dataFields = new HashMap<String, String>();
		for (int i = 0; i < getFields().length; i++) {
			String field = getFields()[i];
			String type = getFormats()[i];
			if (isTimeStampFormat(type)) {
				// GSN doesn't support timestamp data type, all timestamp values
				// are supposed to be bigint.
				dataFields.put(field, "bigint");
			} else if (type.equalsIgnoreCase("numeric")) {
				dataFields.put(field, "numeric");
			} else {
				dataFields.put(field, "string");
			}
		}
		DataField[] toReturn = new DataField[dataFields.size()];
		int i = 0;
		for (String key : dataFields.keySet()) {
			toReturn[i++] = new DataField(key, dataFields.get(key));
		}
		return toReturn;
	}

	public static boolean isTimeStampFormat(String input) {
		return (input.toLowerCase().equalsIgnoreCase("timestamp"));
	}

	public void run() {

		// Register to sources for events
		// for (GadgeonRestServer eachEventSource : eventSources) {
		GadgeonRestServer.addDataListener(this,sensornames);
		 //}
		while (isActive()) {
			if (!listeners.isEmpty() && dataBuffer.size() > 0) {
				JSONObject dataPacket;
				synchronized (dataBuffer) {
					dataPacket = dataBuffer.remove(0);
				}
				// Parse the input
				TreeMap<String, Serializable> inputEvent = parseEvent(dataPacket);

				StreamElement streamElement = new StreamElement(inputEvent,
						getOutputFormat(),manaf);
				logger.warn(inputEvent);
				boolean insertionSuccess = postStreamElement(streamElement);

				
/*				
				String curl1 = "curl -v http://localhost:9080/GadgeonREST/rest/services/"+sensornames+"/"+ manaf +"/"+Double.parseDouble(tempValue)+"/addsensordata -X GET";
				Process p1;

				try {
					p1 = Runtime.getRuntime().exec(curl1);
					p1.waitFor();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.info(e.getMessage());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					logger.info(e.getMessage());
				}


*/				if (!insertionSuccess) {
					logger.error("Insert failed.");
				}

				if (dataBuffer.size() > 0)
					continue;
			}
			try {
				Thread.sleep(1000);

			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			}

		}

	}

	/**
	 * Parse the json object and create a TreeMap
	 * 
	 * @param jsonObj
	 * @return
	 */
	TreeMap<String, Serializable> parseEvent(JSONObject jsonObj) {
		TreeMap<String, Serializable> parsedInput = new TreeMap<String, Serializable>(
				new CaseInsensitiveComparator());

		for (DataField eachField : getOutputFormat()) {
			 tempValue = (String) jsonObj.get(eachField.getName());
			if (tempValue != null && !tempValue.isEmpty()) {
				if (eachField.getType().equalsIgnoreCase("numeric")) {

					try {
						parsedInput.put(eachField.getName(),
								Double.parseDouble(tempValue));
						
					} catch (java.lang.NumberFormatException e) {
						logger.error("Parsing to Numeric fails: Value to parse="
								+ tempValue);
						throw e;
					}


				} else if (eachField.getType().equalsIgnoreCase("string")) {
					parsedInput.put(eachField.getName(), tempValue);
				} else if (eachField.getType().equalsIgnoreCase("timestamp")) {
					try {
						parsedInput.put(eachField.getName(),
								Long.parseLong(tempValue));
					} catch (java.lang.NumberFormatException e) {
						logger.error("Parsing to Timestamp to long (in "
								+ "milliseconds) fails: Value to parse="
								+ tempValue);
						throw e;
					}
				}else
				{
					manaf=Long.parseLong(tempValue);
	



					logger.info("manaaaf-->>"+manaf);					





				}
			}
		}

		return parsedInput;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getWrapperName() {
		return this.getClass().getName();
	}

	@Override
	public DataField[] getOutputFormat() {
		return dataFields;
	}

	@Override
	public void newDataAvailable(JSONObject dataPacket) {

		synchronized (dataBuffer) {
			dataBuffer.add(dataPacket);
		}

	}
}
