package org.openiot.gsn.wrappers.gadgeon.helper;

import java.util.Set;

import javax.ws.rs.core.Application;

import org.openiot.gsn.wrappers.gadgeon.GadgeonRestServer;

import java.util.HashSet;

public class GadgeonApplication extends Application {
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> s = new HashSet<Class<?>>();
		s.add(GadgeonRestServer.class);
		return s;
	}

}
