package org.openiot.gsn.wrappers.gadgeon;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;


import javax.ws.rs.*;

import javax.ws.rs.core.Response;

import org.json.simple.JSONObject;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/iot")
public class GadgeonRestServer {
	
	public static HashMap<String,DataListener> omap=new HashMap<String,DataListener>();
	
	public static int inc=0;
	
	private static final transient Logger logger = LoggerFactory
			.getLogger(GadgeonRestServer.class);
	private static DataListener dataListener = null;

	// public VSManagerService() { super(VSManagerService.class); }
	@POST
	@Path("/{metername}/create")
	public Response createVS(Reader someconfig,
			@PathParam("metername") String meterName) {

		logger.info("Invoking service: " + meterName);
		return Response.ok(meterName).build();
	}

	@POST
	@Path("/{devicetype}/{devicename}/reportEvent")
	public Response reportEvent(Reader deviceValuesJson,
			@PathParam("devicetype") String deviceType,
			@PathParam("devicename") String deviceName) {
		
		logger.info("Invoking service reportEvent: [devicetype= " + deviceType
				+ ",devicename=" + deviceName + "]");

		Response responseValue = Response.ok(deviceType + "_" + deviceName)
				.build();

		try {
			JSONObject jsonObj = (JSONObject) new JSONParser()
					.parse(deviceValuesJson);
			logger.info("reportEvent: Received jsonObj=" + jsonObj.toString());
		

			DataListener wrappername=omap.get(deviceName);
					if(wrappername!=null)
					{	
						dataListener=wrappername;
						// Send the packet to the registered wrapper
						send(jsonObj);
						
					}
					
		} catch (IOException e) {
			logger.error("reportEvent: Error handling JSON input"
					+ deviceValuesJson, e);
			responseValue = Response.serverError().build();
		} catch (ParseException e) {
			logger.error("reportEvent: Error handling JSON input"
					+ deviceValuesJson, e);
			responseValue = Response.serverError().build();
		}

		return responseValue;

	}

	public void send(JSONObject datapacket) {
		if (dataListener != null)
		{
			dataListener.newDataAvailable(datapacket);
		}
		else {
			logger.error("dataListner is not initialized yet. "
					+ "Can not notify any wrapper");
		}
	}

	public static void addDataListener(DataListener wrapper,String sensorname) {
		omap.put(sensorname, wrapper);
	}
	
	
	

}
