
package org.openiot.ui.request.definition.web.model.nodes.impl.aggegators;

import java.io.Serializable;

import org.openiot.ui.request.commons.annotations.Endpoint;
import org.openiot.ui.request.commons.annotations.Endpoints;
import org.openiot.ui.request.commons.annotations.GraphNodeClass;
import org.openiot.ui.request.commons.nodes.base.DefaultGraphNode;
import org.openiot.ui.request.commons.nodes.enums.AnchorType;
import org.openiot.ui.request.commons.nodes.enums.EndpointType;

/**
 *
 * @author Achilleas Anagnostopoulos (aanag) email: aanag@sensap.eu
 */
@GraphNodeClass(label = "Gad_RealTime", type = "AGGREGATOR", scanProperties = true)
@Endpoints({
	 @Endpoint(type = EndpointType.Input, anchorType = AnchorType.Left, scope = "Number Integer Long Double Float sensor_Number sensor_Integer sensor_Long sensor_Double sensor_Float grp_Number grp_Integer grp_Long grp_Double grp_Float", label = "IN", required = true),
	    @Endpoint(type = EndpointType.Output, anchorType = AnchorType.Right, scope = "grp_Date", label = "X", required = true ),
	    @Endpoint(type = EndpointType.Output, anchorType = AnchorType.Right, scope = "agr_Number", label = "Y", required = true )
})
public class Gad_RealTime extends DefaultGraphNode implements Serializable {
	private static final long serialVersionUID = 1L;

    public Gad_RealTime() {
        super();
    }
}
