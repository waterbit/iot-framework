/**
 *    Copyright (c) 2011-2014, OpenIoT
 *   
 *    This file is part of OpenIoT.
 *
 *    OpenIoT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, version 3 of the License.
 *
 *    OpenIoT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with OpenIoT.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Contact: OpenIoT mailto: info@openiot.eu
 */
package org.openiot.ui.request.definition.web.sparql.nodes.base;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.openiot.ui.request.definition.web.sparql.SparqlGenerator;

public class Order extends AbstractSparqlNode implements Serializable{

	private static final long serialVersionUID = 1L;
	SparqlGenerator generator= new SparqlGenerator();

	public Order() {
		super();
	}

	@Override
	public String generate() {
		if( getChildrenCount() == 0 ){
			return "";
		}
		String pad = generatePad(getDepth());
		   if(generator.getGadrealtime().equals("gadrealtime"))
		   {

	    	String s1 = "DESC(";
	    	String s2 = ")";
		    String out = pad + "ORDER BY " +s1+ StringUtils.join(generateChildren(), " ")+s2;
		    return out;
		   }
		   else
		   {
			   String out = pad + "ORDER BY " + StringUtils.join(generateChildren(), " ");
			return out;
		   }
	}

}
