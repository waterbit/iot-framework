

package org.openiot.ui.request.definition.web.sparql.nodes.base;
import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class Limit extends AbstractSparqlNode implements Serializable{

	private static final long serialVersionUID = 1L;
	private String URI;

	public Limit() {
		super();
	}

	@Override
	public String generate() {
		String pad = generatePad(getDepth());
		return pad + "\n "+"LIMIT 50";
	}
}
